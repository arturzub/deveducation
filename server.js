const http = require("http");
const url = require('url');
const fileHandler = require('./model/files_handler');
const controller = require('./controller/controller')

const server = http.createServer((request, response) => {
  request.setEncoding('utf8');
  const parsedURL = url.parse(request.url, true);
  if (parsedURL.pathname === '/persons') {
    switch (request.method) {
      case 'GET':
        response.writeHead(200, { 'Content-Type': 'text/json' });
        controller.getAllItems(parsedURL.query.db).then(items => response.end(JSON.stringify(items)));
      case 'POST':
        response.writeHead(200, { 'Content-Type': 'text/plain' });
        controller.insertData(request, parsedURL.query.db);
        break;
      case 'PUT':
        console.log('PUT');
        response.writeHead(200, { 'Content-Type': 'text/plain' });
        controller.updateData(request, parsedURL.query.db);
        break;
      case 'DELETE':
        response.writeHead(200, { 'Content-Type': 'text/plain' });
        controller.deleteData(parsedURL.query.id, parsedURL.query.db);
        break;
    }
  } else {
    fileHandler.getFilesRequest(request, response);
  }

})

server.listen(3000, () => {
  console.log('Сервер запущен...')
})


