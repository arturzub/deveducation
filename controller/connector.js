const mySql = require('../model/config_mysql');
const mock = require('../model/config_mock');
const jsonHandler = require('../model/config_json');
const fireBase = require('../model/config_firebase');

module.exports = {
  async getAllItems(db) {
    let persons = [];
    switch (db) {
      case 'mock':
        persons = await mock.getAllItems();
        break;
      case 'json':
        persons = await jsonHandler.getAllItems();
        break;
      case 'mysql':
        persons = await mySql.getAllItems();
        break;
      case 'firebase':
        persons = await fireBase.getAllItems();
        break;
    }
    return persons;
  },
  insertData(data, db) {
    switch (db) {
      case 'mock':
        mock.insertData(data);
        break;
      case 'json':
        jsonHandler.insertData(data);
        break;
      case 'mysql':
        mySql.insertData(data);
        break;
      case 'firebase':
        fireBase.insertData(data);
        break;
    }
  },
  updateData(data, db) {
    switch (db) {
      case 'mock':
        mock.updateData(data);
        break;
      case 'json':
        jsonHandler.updateData(data);
        break;
      case 'mysql': mySql.updateData(data);
        break;
      case 'firebase':
        fireBase.updateData(data);
        break;
    }
  },
  deleteData(id, db) {
    switch (db) {
      case 'mock':
        mock.deleteData(id);
        break;
      case 'json':
        jsonHandler.deleteData(id);
        break;
      case 'mysql': mySql.deleteData(id);
        break;
      case 'firebase':
        fireBase.deleteData(id);
        break;
    }
  }


}