const mySql = require('../model/config_mysql');
const connector = require('./connector');

module.exports = {
    getAllItems(db) {
        return connector.getAllItems(db);
    },
    insertData(request, db) {
        const body = [];
        request.on('data', data => {
            body.push(Buffer.from(data));
            if (body.length > 0) {
                let item = JSON.parse((body.toString()));
                connector.insertData(item, db);
            }

        })
    },
    updateData(request, db) {
        const body = [];
        request.on('data', data => {
            body.push(Buffer.from(data))
            if (body.length > 0) {
                let item = JSON.parse((body.toString()));
                connector.updateData(item, db);
            }
        })
    },
    deleteData(id, db) {
        connector.deleteData(id, db);
    }
}
