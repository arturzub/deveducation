class CRUDCore {

  constructor(dataBase) {
    this.db = dataBase;
  }


  findDataIdDb (id) {
    let Id = null;
    this.db.forEach(function (item,i) {
      if (item.id === id) {
        Id = item.id;
      }
    });

    if (Id !== null) {
      return Id;
    }
    return false;
  }

  findDataIndexDb(id) {
    let index = null;
    this.db.forEach(function (item,i) {
      if (item.id === id) {
        index = i;
      }
    });

    if (index !== null) {
      return index;
    }
    return false;
  }


  essencePerson (data) {
    let scope = {};
    data.forEach(function (item) {
      scope[item.name] = item.value;
    });
    return scope;
  }



}

