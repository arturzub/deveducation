class CRUDSystem extends CRUDCore{

  constructor(dataBase) {
    super(dataBase);
    this.db = dataBase;

  }

  create(data) {
    let person = this.essencePerson(data);

    if (this.findDataIdDb(person.id) || person.id === '') {
      return false;
    }
    d(this.db);
    this.db.push(person);

    return true;
  }

  read(nameDb) {
    const json = localStorage.getItem(nameDb);
    const value = JSON.parse(json);
    if (value.length === this.db.length) {
      return false;
    }
    localStorage.removeItem(nameDb);
    return value;
  }

  update(data) {
    const person = this.essencePerson(data);
    let index = null;
    if (!this.findDataIdDb(person.id)) {
      return index;
    }

    index = this.findDataIndexDb(person.id);

    this.db[index] = person;

    return index;
  }

  delete(id) {
    let memoryId = this.findDataIdDb(id);
    let index = this.findDataIndexDb(id);

    if (this.findDataIdDb(id)) {
      this.db.splice(index,1);
    }

    return memoryId ;
  }

  clearTable () {

    this.db.splice(0);

    return true;
  }

  addStart() {
    let person = this.essencePerson(data);

    if (this.findDataIdDb(person.id) || person.id === '') {
      return false;
    }
    this.db.unshift(person);

    return true;
  }

  addMiddle() {
    let person = this.essencePerson(data);

    if (this.findDataIdDb(person.id) || person.id === '') {
      return false;
    }
    let middleDb = this.db.length / 2;
    this.db.splice(middleDb,0,person);
    d(this.db);
    return true;
  }

  save(name,data) {
    if (!data || !name) {
      return false;
    }
    localStorage.removeItem(name);
    localStorage.setItem(name, JSON.stringify(data));

    return true;
  }


}

