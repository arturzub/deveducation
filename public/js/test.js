describe('CRUDSystem', function () {

  let dataBase = [
    {id: "1", fname: "Artur", lname: "Zub", country: "Dnepr", age: "26"},
    {id: "2", fname: "Andrey", lname: "Lazarev", country: "Kyiv", age: "30"},
    {id: "3", fname: "Viktoritya", lname: "Zub", country: "Dnepr", age: "25"}
  ];
  const data = document.querySelectorAll('.input_form');

  let id = data[0],
      fname = data[1];

  const CRUD = new CRUDSystem(dataBase);

  describe('CRUDSystem', function () {
    describe('create',function () {
      it('Совпадениие по id', function () {
        let id = data[0];
        id.value = 1;

        const actual = CRUD.create(data);
        const expected = false;

        assert.strictEqual(actual, expected);
      });
      it('Добавление записи', function () {
        let id = data[0];
        id.value = 12;
        let memoryLength = dataBase.length + 1;
        CRUD.create(data);

        const actual = memoryLength;
        const expected = dataBase.length ;

        assert.strictEqual(actual, expected);
      });
    });
    describe('update',function () {
      it('id не записан ', function () {
        id.value = 4;
        const actual = CRUD.update(data);
        const expected = null;

        assert.strictEqual(actual, expected);
      });
      it('Обновление данных прошло успешно', function () {
        id.value = 1;
        fname.value = 'aaa';

        const actual = 0;
        const expected = CRUD.update(data) ;

        assert.strictEqual(actual, expected);
      });
    });
    describe('delete',function () {
        it('ID не найден', function () {
          const id = document.getElementById('id_input');
          id.value = 5;

          const actual = CRUD.delete(id.value);
          const expected = false;

          assert.strictEqual(actual, expected);
        });
      it('Данные удалины ', function () {
        const id = document.getElementById('id_input');
        id.value = 1;

        const actual = CRUD.delete(id.value);
        const expected = id.value;

        assert.strictEqual(actual, expected);
      });
    });
    describe('clearTable',function () {
      it('Вседа данные удаленны', function () {
        let dataBase = [
          {id: "1", fname: "Artur", lname: "Zub", country: "Dnepr", age: "26"},
          {id: "2", fname: "Andrey", lname: "Lazarev", country: "Kyiv", age: "30"},
          {id: "3", fname: "Viktoritya", lname: "Zub", country: "Dnepr", age: "25"}
        ];
        const CRUD = new CRUDSystem(dataBase);

        const actual = CRUD.clearTable();
        const expected = true;

        assert.strictEqual(actual, expected);
      });
    });
    describe('addStart',function () {
      it('Добавление записи с индексом 0', function () {
        let id = data[0];
        id.value = 13;

        const actual = CRUD.addStart(data);
        const expected = id.value;

        assert.strictEqual(actual, expected);
      });
      it('Id не найден', function () {
        let id = data[0];
        id.value = 2;

        const actual = CRUD.addStart(data);
        const expected = false;

        assert.strictEqual(actual, expected);
      });
    });
    describe('addMiddle',function () {
      it('Добавление записи в середину', function () {
        id.value = 17;

        const actual = CRUD.addMiddle(data);
        const expected = true;

        assert.strictEqual(actual, expected);
      });
      it('Совпадениие по id', function () {
        let id = data[0];
        id.value = 2;

        const actual = CRUD.addMiddle(data);
        const expected = false;

        assert.strictEqual(actual, expected);
      });
    });
    describe('save',function () {
      it('Добавление localStorage', function () {

        const actual = CRUD.save("dataBase", dataBase);
        const expected = true;

        assert.strictEqual(actual, expected);
      });
    });
    describe('read',function () {
      it('localStorage нету сопвпадений ', function () {

        CRUD.save("dataBase", dataBase);

        const actual = CRUD.read("qwerty");
        const expected = false;

        assert.strictEqual(actual, expected);
      });
      it('localStorage найдена запись ', function () {

        CRUD.save("dataBase", dataBase);

        const actual = CRUD.read("dataBase");
        const expected = dataBase;

        assert.deepEqual(actual, expected);
      });
    });
  });

});
