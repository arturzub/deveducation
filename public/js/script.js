const data = document.querySelectorAll('.input_form');
const id = document.getElementById('id_input');
const fname = document.getElementsByName('first_name');
const lname = document.getElementsByName('last_name');
const age = document.getElementsByName('age');
const buttons = document.querySelectorAll('.crud_button');
const table = document.getElementById('main_table');
const tbody = table.querySelector('#main_table > tbody');
const dbSelect = document.getElementsByName('dataBase')[0];

let memoryDb;

table.addEventListener('click', addDataToInput);

function addDataToInput(e) {
  let id = Number(e.target.parentNode.id);
  let findItem = null;
  findItem = memoryDb.find(item => Number(item.id) === id);
  console.log(findItem);


  if (findItem) {
    data.forEach(function (item) {
      item.value = findItem[item.name];
    });
  }
  return true;
}

dbSelect.addEventListener('change', currentDb);
function currentDb() {
  localStorage.setItem('dbName', dbSelect.value);
}
buttons.forEach(function (item) {
  item.addEventListener('click', function (e) {
    e.preventDefault;
    switch (item.innerText) {
      case 'Create': sendCreate(); window.location.href = "/";
        break;
      case 'Read': window.location.href = "/";
        break;
      case 'Update': sendUpdate(); window.location.href = "/";
        break;
      case 'Delete': sendDelete(); window.location.href = "/";
        break;
    }
  })
})
document.querySelector('form').addEventListener('click', function (e) {
  e.preventDefault();
});


window.addEventListener('load', onLoadData);

function onLoadData() {
  parsePersons(tbody);
  createDataSelect(dbSelect);
}

async function parsePersons(container) {
  if (!container) {
    return void 0;
  }

  const response = await getPersons();
  memoryDb = response.data;
  if (response.data.length > 0) {
    for (const person of response.data) {
      addRow(container, person);
    }
  }
}

function addRow(node, person) {
  if (!person) {
    return void 0;
  }
  const tr = createRow(person);
  node.appendChild(tr);
}

function createRow(data) {
  if (!data) {
    return void 0;
  }

  const tr = document.createElement('tr');

  tr.id = data.id;
  let content = `<td>${data.id}</td><td>${data.first_name}</td><td>${data.last_name}</td><td>${data.age}</td>`;
  tr.innerHTML = content;

  return tr;
}

//request
async function getPersons() {
  try {
    return await axios.get(`/persons?db=${getSaveDbSelect()}`);
  } catch (error) {
    console.log(error);
  }
}

async function sendCreate() {
  try {
    await axios.post(`/persons?db=${getSaveDbSelect()}`, getCurrentData());
  } catch (error) {
    console.log(error);
  }
}

async function sendUpdate() {
  try {
    await axios.put(`/persons?db=${getSaveDbSelect()}`, getCurrentData());
  } catch (error) {
    console.log(error);
  }
}
async function sendDelete() {
  try {
    await axios.delete(`/persons?db=${getSaveDbSelect()}&id=${getCurrentData().id}`);
  } catch (error) {
    console.log(error);
  }
}

//HELPERS
function getSaveDbSelect() {
  let db = 'mysql';
  if (localStorage.getItem('dbName')) {
    db = localStorage.getItem('dbName').toLowerCase();
  }
  return db;
}

function getCurrentData() {
  return {
    id: id.value,
    first_name: fname[0].value,
    last_name: lname[0].value,
    age: age[0].value
  }
}

function createDataSelect(db) {
  const optionItem = [
    'MySql',
    'JSON',
    'Mock',
    'PostreSQL',
    'SQLlite',
    'Firebase'
  ];

  const options = optionItem.map(item => {
    let selected = ''
    if (localStorage.getItem('dbName') === item) {
      selected = 'selected';
    }
    return `<option ${selected}>${item}</option>`;
  })
  dbSelect.insertAdjacentHTML('afterbegin', options.join(' '));

}

// requests

const ajaxSend = (data, method, id) => {
  const url = id ? `/persons?id=${id}` : `/persons`;

  fetch(url, { // файл-обработчик 
    method: method,
    headers: {
      'Content-Type': 'application/json', // отправляемые данные 
    },
    body: JSON.stringify(data)
  })
    .then(response => onDispalay('Сообщение отправлено'))
    .catch(error => console.error(error))

};
function onDispalay(response) {
  console.log(response)
}



