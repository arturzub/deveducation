class CRUDCore {

    constructor(dataBase) {
      this.db = dataBase;
    }
  
  
    findDataIdDb (id) {
      let Id = null; 
      this.db.forEach(function (item) {

        if (item.id === id) {
          
          return Id = item.id;
        }
      });
      return false;
    }
  
    findDataIndexDb(id) {
      let uid = Number(id);
      let index = null;
      this.db.forEach(function (item,i) {
        if (item.id === uid) {
          index = i;
        }
      });
      if (index !== null) {
        return index;
      }
      return false;
    }
  }
  
  module.exports = class CRUDSystem extends CRUDCore{
  
    constructor(dataBase) {
      super(dataBase);
      this.db = dataBase;
      this.count = 0;  
    }

    counter() {
      this.count++;
      return this.count;
    }
  
    create(data) {
      data.id = this.counter();

      if (this.findDataIdDb(data.id) || data.id === '') {
        return false;
      }      
     this.db.push(data);
  
      return true;
    }
  
    read(nameDb) {
      return this.db;
    }
  
    update(data) {

      let index = this.findDataIndexDb(data.id);
      this.db[index] = data;
  
      return index;
    }
  
    delete(id) {
      let index = this.findDataIndexDb(id);
      console.log(this.db);
      
      if (index > 0) {
        this.db.splice(index,1);
      } else {
        this.db.pop();
      }
    }
  
  }
  
  