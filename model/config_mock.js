const mockDb = require('../database/mock');
const CRUDSystem = require('../class/mock_class_crud');
const crud = new CRUDSystem(mockDb);

module.exports = {
    getAllItems() {
        return crud.read();
    },
    insertData(data) {        
        crud.create(data);
    },
    updateData(data) {        
        crud.update(data);
    },
    deleteData(id) {
        crud.delete(id);
    }
}