const mysql = require("mysql2");

const connection = mysql.createConnection({
  host: "localhost",
  user: "root",
  database: "crud_persons",
  password: "",
});
connection.connect((err) => {
  if (err) {
    return console.error("Ошибка: " + err.message);
  }
  else {
    console.log("Подключение к серверу MySQL успешно установлено!");
  }
});

module.exports = {
  getAllItems() {
    return query('SELECT * FROM persons');
  },
  insertData(item) {
    data = [item.first_name, item.last_name, item.age];
    let query = "INSERT INTO `persons` (`id`, `first_name`, `last_name`, `age`) VALUES (NULL, ?, ?, ?)";
    connection.query(query, data);
  },
  updateData(data) {
    let arrData = [data.first_name, data.last_name, data.age, data.id]
    let query = `UPDATE persons SET first_name=?, last_name=?, age=? WHERE id=?`;
    connection.query(query,arrData);
  },
  deleteData(id) {
    let query = `DELETE FROM persons WHERE persons.id = ${+(id)}`;
    connection.query(query);
  }
}

function query(sql, data) {
  return new Promise((resolve, reject) => {
    connection.query(sql, data, (error, result) => {
      if (error) {
        return reject(error)
      }
      resolve(result);
    });
  }).catch(error => console.log(error));
}





