const fs = require('fs');
const path = require('path');
const jsDb = path.join(__dirname, '../database/json.js');
const charset = 'utf-8';
let count = 1;

module.exports = {
  getAllItems() {
    return new Promise((resolve, reject) => {
      fs.readFile(jsDb, charset, (error, data) => {
        if (error) {
          console.log("Ошибка чтения файла", err)
          return
        }
        try {
          resolve(JSON.parse(data));
        } catch (error) {
          console.log('Ошибка чтения JSON:', error)
        }
      });
    });
  },
  insertData(person) {
    return new Promise((resolve, reject) => {
      fs.readFile(jsDb, charset, (error, data) => {

        let persons = JSON.parse(data);
        let maxPId = counter();

        person.id = maxPId;
        persons.push(person);
        fs.writeFileSync(jsDb, JSON.stringify(persons));

        try {
          resolve(person.id);
        } catch (error) {
          console.log('Ошибка чтения JSON:', error)
        }
      });
    });
  },
  updateData(editedPerson) {
    return new Promise((resolve, reject) => {
      fs.readFile(jsDb, charset, (error, data) => {

        let persons = JSON.parse(data);

        persons = persons.map(person => {
          return (person.id == editedPerson.id) ? editedPerson : person;
        });

        fs.writeFileSync(jsDb, JSON.stringify(persons));

        try {
          resolve(true);
        } catch (error) {
          console.log('Ошибка чтения JSON:', error)
        }
      });
    });
  },
  deleteData(id) {
    return new Promise((resolve, reject) => {
      fs.readFile(jsDb, charset, (error, data) => {
        let persons = JSON.parse(data);

        persons = persons.filter(person => person.id != id);

        fs.writeFileSync(jsDb, JSON.stringify(persons));

        try {
          resolve(true);
        } catch (error) {
          console.log('Ошибка чтения JSON:', error)
        }
      });
    });
  }



}

function counter() {
  count++;
  return count;
}